#!/usr/bin/env python3


import argparse
import re
import os
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

parser = argparse.ArgumentParser(description='Plot data distribution.')
parser.add_argument('file', type=str, nargs=1,
                    help='data file')

TIME_PATTERN_DEFAULT = r"^(?P<h>\d\d):(?P<m>\d\d):(?P<s>\d\d)\.(?P<ms>\d\d\d).*"
parser.add_argument(
    '--time_pattern', type=str,
    default=TIME_PATTERN_DEFAULT,
    help='parsing time pattern'
         '; available units: h - hour, m - minut, s - second,'
         ' ms - millisecond'
         '; default "{}"'.format(TIME_PATTERN_DEFAULT))

parser.add_argument(
    '--interval', type=str, default='1s',
    help='time interval e.g. 1m, 2s, 10ms'
         '; all units supported from time_pattern option'
         '; data from time_pattern will be used in plot')

parser.add_argument(
    '--title', type=str, default='',
    help='Chart title')

parser.add_argument(
    '--range', type=str, default='',
    help='Time range in form of start-end'
         '; time format to be parsed by time_pattern')

parser.add_argument(
    '--mode', type=str, default='show',
    help='Output mode; available: show, img')

VALUE_PATTERN_DEFAULT = r'.*msg:\s(?P<val>[a-z\d]+)\s.*'
parser.add_argument('--value_pattern', type=str,
                    default=VALUE_PATTERN_DEFAULT,
                    help='value pattern; "val" group must be defined'
                         '; default: "{}"'.format(VALUE_PATTERN_DEFAULT))


def parse_interval(arg):
    INTERVAL_RE = re.compile(r'(?P<val>\d+)(?P<unit>[msh]+)')
    m = INTERVAL_RE.match(arg)
    if m:
        return (int(m.group('val')), m.group('unit'))


def parse_time(line, time_re):
    m = time_re.match(line)
    if m is not None:
        return {'h': int(m.group('h')), 'm': int(m.group('m')),
                's': int(m.group('s')), 'ms': int(m.group('ms'))}
    return None


MAX_TIME_PER_UNIT = {'h': 24, 'm': 60, 's': 60, 'ms': 1000}


def next_time(time, interval):
    time = time
    interval, unit = interval
    time[unit] += interval

    def fix(parent_unit, unit, time):
        time[parent_unit] += int(time[unit]/MAX_TIME_PER_UNIT[unit])
        time[unit] = time[unit] % MAX_TIME_PER_UNIT[unit]

    fix('s', 'ms', time)
    fix('m', 's', time)
    fix('h', 'm', time)
    return time


def format_time(time):
    return '{:02d}:{:02d}:{:02d}.{:03d}'.format(
        time['h'], time['m'], time['s'], time['ms'])


def is_older_then(time1, time2):
    return time1['h'] < time2['h']\
        or (time1['h'] == time2['h'] and time1['m'] < time2['m'])\
        or (time1['h'] == time2['h'] and time1['m'] == time2['m'] and time1['s'] < time2['s'])\
        or (time1['h'] == time2['h'] and time1['m'] == time2['m'] and time1['s'] == time2['s'] and time1['ms'] < time2['ms'])


def extract_counts(all_counts, name):
    values = list()
    for count in all_counts:
        if name not in count:
            values.append(0)
        else:
            values.append(count[name])
    return values


def strip_data(lines, trange, time_re):
    if len(trange) == 0:
        return lines
    stripped = list()
    start, end = trange.split('-')
    start = parse_time(start, time_re)
    end = parse_time(end, time_re)
    for line in lines:
        time = parse_time(line, time_re)
        if is_older_then(time, end) and not is_older_then(time, start):
            stripped.append(line)
    print(stripped)
    return stripped


ARGS = parser.parse_args()
TIME_RE = re.compile(ARGS.time_pattern)
VALUE_RE = re.compile(ARGS.value_pattern)
INTERVAL = parse_interval(ARGS.interval)

with open(ARGS.file[0]) as file:
    lines = strip_data(file.readlines(), ARGS.range, TIME_RE)
    start = parse_time(lines[0], TIME_RE)
    end = next_time(start, INTERVAL)
    values = set()
    times = list()
    counts = list()
    interval_counts = dict()
    for line in lines:
        if not is_older_then(parse_time(line, TIME_RE), end):
            times.append(format_time(end))
            counts.append(interval_counts)
            interval_counts = dict()
            start = end
            end = next_time(start, INTERVAL)
        value_matched = VALUE_RE.match(line)
        if value_matched:
            val = value_matched.group('val')
            values.add(val)
            if val not in interval_counts:
                interval_counts[val] = 0
            interval_counts[val] += 1

    fig, ax = plt.subplots(1, 1)
    fig.set_size_inches(max(len(counts)/10, 30), 20)
    ax.grid(color='grey', linestyle='-', linewidth=0.5)
    fig.autofmt_xdate()
    fig.suptitle(ARGS.title, fontsize=32)
    plots = []
    for name in values:
        plot, = plt.plot(times, extract_counts(counts, name), label=name)
        plots.append(plot)


    if ARGS.mode == 'img':
        filename = "{}.svg".format(
            os.path.splitext(os.path.basename(ARGS.file[0]))[0])
        plt.xlabel('time', fontsize=20)
        plt.ylabel('rate/{}'.format(ARGS.interval), fontsize=20)
        plt.legend(handles=plots, fontsize=20)
        ax.xaxis.set_major_locator(ticker.MultipleLocator(max(1, len(counts)/100)))
        fig.savefig(filename, format="svg")
        print('Image generated:', filename)
    if ARGS.mode == 'show':
        plt.xlabel('time', fontsize=10)
        plt.ylabel('rate/{}'.format(ARGS.interval), fontsize=10)
        plt.legend(handles=plots, fontsize=10)
        ax.xaxis.set_major_locator(ticker.MultipleLocator(max(1, len(counts)/50)))
        plt.show()

